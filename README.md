# Smprofiles
This repository will contains some mini projects in multiple backend frameworks.

## List
- railsapp/ Contains the Ruby on Rails implementation.
- phoenixapp/ Contains the Elixir Phoenix implementation.
- amberapp/ Contains the Crystla Amber implementation.
