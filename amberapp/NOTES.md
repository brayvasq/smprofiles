# amberapp

[![Amber Framework](https://img.shields.io/badge/using-amber_framework-orange.svg)](https://amberframework.org)

This is a project written using [Amber](https://amberframework.org). Enjoy!

## Getting Started

These instructions will get a copy of this project running on your machine for development and testing purposes.

Please see [deployment](https://docs.amberframework.org/amber/deployment) for notes on deploying the project in production.

## Prerequisites

This project requires [Crystal](https://crystal-lang.org/) ([installation guide](https://crystal-lang.org/docs/installation/)).

## Development

To start your Amber server:

1. Install dependencies with `shards install`
2. Build executables with `shards build`
3. Create and migrate your database with `bin/amber db create migrate`. Also see [creating the database](https://docs.amberframework.org/amber/guides/create-new-app#creating-the-database).
4. Start Amber server with `bin/amber watch`

Now you can visit http://localhost:3000/ from your browser.

Getting an error message you need help decoding? Check the [Amber troubleshooting guide](https://docs.amberframework.org/amber/troubleshooting), post a [tagged message on Stack Overflow](https://stackoverflow.com/questions/tagged/amber-framework), or visit [Amber on Gitter](https://gitter.im/amberframework/amber).

Using Docker? Please check [Amber Docker guides](https://docs.amberframework.org/amber/guides/docker).

## Tests

To run the test suite:

```
crystal spec
```

## Contributing

1. Fork it ( https://github.com/your-github-user/amberapp/fork )
2. Create your feature branch ( `git checkout -b my-new-feature` )
3. Commit your changes ( `git commit -am 'Add some feature'` )
4. Push to the branch ( `git push origin my-new-feature` )
5. Create a new Pull Request

## Contributors

- [your-github-user](https://github.com/your-github-user) brayvasq - creator, maintainer

# Guide

## Amber App

instalando crystal 

https://crystal-lang.org/reference/installation/

Instalando amber

https://docs.amberframework.org/amber/getting-started

Creando proyecto

```bash
amber new amberapp
```

Instalando dependecias

```bash
shards install
```

```bash
npm install
```

**Configurando DB**

Amber trabaja con postgresql por defecto, la configuración para el entorno de desarrollo es `amberapp/config/environments/development.yml`, la linea que indica la conexión a la base de datos es la siguiente

```yaml
database_url:....
```

Crear base de datos

```bash
amber db create
```

Ejecutar proyecto

```bash
amber watch
```

## Añadir usuarios a la aplicación

Generando autenticación : https://dev.to/eliasjpr/recipebuilding-an-amber-authentication-system-517

https://docs.amberframework.org/amber/examples/amber-auth

```bash
amber g auth User
```

Añadir campos al usuario, modificar la migración `amberapp/db/migrations/20190930004319178_create_user.sql`

```sql
-- +micrate Up
CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  email VARCHAR,
  name VARCHAR,
  last_name VARCHAR,
  description VARCHAR,
  age INTEGER,
  date_of_birth DATE,
  country VARCHAR,
  job VARCHAR,
  hashed_password VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS users;
```

En el modelo `amberapp\src\models\user.cr` añadir los siguientes campos.

```crystal
  column email : String?
  column name  : String?
  column last_name : String?
  column description : String?
  column age : Int32
  column date_of_birth : Time
  column country : String?
  column job     : String?
```



Migraciones

```bash
amber db migrate
```

Ver rutas

```bash
amber routes
```

Añadir ruta listado de usuarios `amberapp\config\routes.cr`

```crystal
get "/users", UserController, :index
```

Añadir acción en UserController `amberapp\src\controllers\user_controller.cr`

```crystal
before_action do
    only [:edit, :update] { redirect_signed_out_user }
  end

  def index
    users = User.all
    render("index.slang")
  end
```

Menú `amberapp\src\views\layouts\application.slang`

```crystal
doctype html
html
  head
    title Amberapp using Amber
    meta charset="utf-8"
    meta http-equiv="X-UA-Compatible" content="IE=edge"
    meta name="viewport" content="width=device-width, initial-scale=1"
    link rel="stylesheet" href="/dist/main.bundle.css"
    link rel="apple-touch-icon" href="/favicon.png"
    link rel="icon" href="/favicon.png"
    link rel="icon" type="image/x-icon" href="/favicon.ico"

  body
    header
      nav.navbar.navbar-expand.navbar-light.bg-light
        .container
          a.navbar-brand href="/"
            img src="/dist/images/logo.svg" height="30" alt="Amber logo"
              | SmProfiles
          ul.navbar-nav.mr-auto
            == render(partial: "layouts/_nav.slang")
          ul.navbar-nav
              == render(partial: "layouts/_session.slang")

    .container
      - flash.each do |key, value|
        div class="alert alert-#{key}" role="alert"
          = flash[key]

    == content

    footer.py-4.bg-light.text-black-50
      div.container.text-center
        small
          | Copyright &copy; Your Website


    script src="/dist/main.bundle.js"

    - if Amber.settings.auto_reload?
      script src="/js/client_reload.js"
```

El archivo de estilos contendrá lo siguiente

```scss
$primary: #e67e22;
$secondary: #d35400;

@import '~bootstrap/scss/bootstrap';

.btn-primary {
  background-color: $primary;
  border-color: $primary;
}

.btn-primary:hover, .btn-primary:focus{
  background-color: $secondary;
  border-color: $secondary;
}

.navbar{
  background-color: rgb(240, 242, 245);
  box-shadow: rgba(0, 0, 0, 0.15) 0px 1px 0px 0px;
  height: 36px;
}

.btn-sm{
  width: 100px;
}

.navbar-image{
  width: 24px;
  height: 24px;
  border-radius: 100%;
  margin-top: -5px;
  margin-left: -5px;
  margin-right: 5px;
}

header{
  z-index: 1;
  position: fixed;
  width: 100%;
  top: 0;
}

.my-container{
  display: flex;
  justify-content: center;
  align-content: center;
  padding: 100px;
}

.my-container .card{
  width: 100%;
}

.users-section h3, .user-container a{
  text-align: center;
}

.user-container {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5%;
}

.more-profiles{
  position: relative;
  top: 50%;
  display: flex !important;
  justify-content: center;
  align-items: center;
  width: 50%;
}

.block{
  width: 100%;
  height: 700px;
  background: linear-gradient(150deg, $primary , #ffffff);
  position: absolute;
}

.basic-info .list-group-item{
  padding: 0;
}

.div-actions{
  float: right;
}

.edit-list li{
  list-style-type: none;
}

.user-img{
  height: 256px;
}

.text {
  position: relative;
  height: 760px;
  display: flex;
  margin: -60px 0 0 150px;
  flex-direction: column;
  justify-content: center;
  color: #ffffff;
  max-width: 1024px;
}
.text h1 {
  font-size: 40px;
  opacity: 1;
}
.text p {
  color: #ffffff;
  font-size: 20px;
  max-width: 50%;
  font-size: 17px;
  line-height: 28px;
  opacity: 1;
}
```

Botones de sesión `amberapp\src\views\layouts\_session.slang`

```crystal
- if (current_user = context.current_user)
  - active = context.request.path == "/profile" ? "active" : ""
  li.nav-item class=active
    a.btn.btn-sm href="/profile"
      == current_user.email
  li.nav-item
    a.btn.btn-sm.btn-primar href="/signout" Sign Out
- else
  - active = context.request.path == "/signin" ? "active" : ""
  li.nav-item class=active
    a.btn.btn-sm href="/signin" Sign In
  - active = context.request.path == "/signup" ? "active" : ""
  li.nav-item class=active
    a.btn.btn-sm.btn-primary href="/signup" Sign Up
```

Links al home `amberapp\src\views\layouts\_nav.slang`

```crystal
- active = context.request.path == "/" ? "active" : ""
li.nav-item class=active
  a.nav-link href="/" Home

li.nav-item
  a.nav-link href="/" Features

li.nav-item
  a.nav-link href="/" Pricing
```

Crear partial de error `amberapp\src\views\layouts\_errors.slang`

```crystal
.container
  - flash.each do |key, value|
    div class="alert alert-#{key}" role="alert"
      = flash[key]

```

Pagina principal `amberapp\src\views\home\index.slang`

```crystal
section
  div.block
  div.text
    h1
      | Become known, get your profile
    p
      | Smtodo is an application where you can describe yourself and explore profile of million of users.

    div
      a.btn.btn-info href="/signup" Get Started

  div.users-section
    h3
      | Some Users
    div.user-container
      div.card-deck
        == link_to "Explore Users", "/users"

.row.justify-content-center
  .col-sm-12.col-md-6
    h2 = t "welcome_to_amber"
    p Thank you for trying out the Amber Framework.  We are working hard to provide a super fast and reliable framework that provides all the productivity tools you are used to without sacrificing the speed.
    .list-group
      a.list-group-item.list-group-item-action target="_blank" href="https://docs.amberframework.org" Getting Started with Amber Framework
      a.list-group-item.list-group-item-action target="_blank" href="https://github.com/veelenga/awesome-crystal" List of Awesome Crystal projects and shards
      a.list-group-item.list-group-item-action target="_blank" href="https://crystalshards.xyz" What's hot in Crystal right now
```

Vista sign in o login `amberapp\src\views\session\new.slang`

```crystal
section
  == render(partial: "layouts/_errors.slang")
  div.container.my-container
    div.card
      div.card-body
        h5.card-title
          | Log In
        div.col-md-6.col-xs-6.col-sm-12.col-6.float-left
          - if user.errors
            ul.errors
            - user.errors.each do |error|
              li = error.to_s

          form action="/session" method="post"
            == csrf_tag
            .form-group
              == label :email
              input.form-control type="email" name="email" placeholder="Email"
            .form-group
              == label :password
              input.form-control type="password" name="password" placeholder="Password"

            div
              button.btn.btn-success.btn-sm type="submit" Sign In
              == link_to("Don't have an account yet?", "/signup")
        div.col-md-6.col-xs-6.col-sm-6.col-6.float-right
          div
            h5
              | Your password is your master encryption key.
            p
              | Your account’s security depends on the strength of your password. Passwords that are too short, too simple or consist of dictionary words are easily guessed.
          div
            h5
              | Your account is only as secure as your computer.
            p
              | Never enter your password on a device that you do not fully trust. Do not log into your account from a shared or public computer. For enhanced security, install our browser extension and load MEGA from your computer rather than from our servers.
```

Vista de registro `amberapp\src\views\registration\new.slang`

```ruby
section
  == render(partial: "layouts/_errors.slang")
  div.container.my-container
    div.card
      div.card-body
        h5.card-title
          | Sign Up
        div.col-md-6.col-xs-6.col-sm-12.col-6.float-left
          - if user
            - if user.errors
              ul.errors
              - user.errors.each do |error|
                li = error.to_s
          - if @validation_errors
            - @validation_errors.not_nil!.each do |error|
              li = error.message

          form action="/registration" method="post"
            == csrf_tag
            .form-group
              == label :email
              input.form-control type="email" name="email" placeholder="Email"
            .form-group
              == label :password
              input.form-control type="password" name="password" placeholder="Password"

            div
              button.btn.btn-success.btn-sm type="submit" Register
              == link_to("Already have an account?", "/signin")
        div.col-md-6.col-xs-6.col-sm-6.col-6.float-right
          div
            h5
              | Your password is your master encryption key.
            p
              | Your account’s security depends on the strength of your password. Passwords that are too short, too simple or consist of dictionary words are easily guessed.
          div
            h5
              | Your account is only as secure as your computer.
            p
              | Never enter your password on a device that you do not fully trust. Do not log into your account from a shared or public computer. For enhanced security, install our browser extension and load MEGA from your computer rather than from our servers.
```

Profile view `amberapp\src\views\user\show.slang`

```crystal
section
  div.container.my-container.col-md-8
    div.card
      div.row.no-gutters
        div.col-md-4
          img.card-img src="/dist/images/default-men.png" height="30" alt="profile logo"
        div.col-md-8
          div.card-body
            div.basic-info
              h5.card-title
                | Basic info
              ul.list-group.list-group-flush
                li.list-group-item
                  p
                    b Name :
                    == user.name
                li.list-group-item
                  p
                    b Last Name :
                    == user.last_name
                li.list-group-item
                  p
                    b Email :
                    == user.email
                li.list-group-item
                  p
                    b Age :
                    == user.age
                li.list-group-item
                  p
                    b Date of Birth :
                    == user.date_of_birth

                li.list-group-item
                  p
                    b Country :
                    == user.country

                li.list-group-item
                  p
                    b Job :
                    == user.job

      div.row.no-gutters
        div.card-body
          div
            h5 Description
            p == user.description
          div
            == link_to("Edit", "/profile/edit", class: "btn btn-success btn-sm")
```

Edit profile `amberapp\src\views\user\edit.slang`

```crystal
section
  div.container.my-container.col-md-8
    - if user.errors
      ul.errors
      - user.errors.each do |error|
        li = error.to_s
    div.card
      == form(action: "/profile", method: :patch) do
        == csrf_tag
        div.row.no-gutters
          div.col-md-4
            img.card-img src="/dist/images/default-men.png" height="30" alt="profile logo"
          div.col-md-8
            div.card-body
              div.basic-info
                h5.card-title Edit info
                ul.list-group.list-group-flush.edit-list
                  li
                    div.field
                      input.form-control type="text" name="name" placeholder="Name" value="#{user.name}"
                  li
                    div.field
                      input.form-control type="text" name="last_name" placeholder="Last Name" value="#{user.last_name}"
                  li
                    div.field
                      input.form-control type="email" name="email" placeholder="Email" value="#{user.email}"
                  li
                    div.field
                      input.form-control type="text" name="age" placeholder="Age" value="#{user.age}"
                  li
                    div.field
                      input.form-control type="date" name="date_of_birth" placeholder="Date of Birth" value="#{user.date_of_birth}"
                  li
                    div.field
                      input.form-control type="text" name="country" placeholder="Country" value="#{user.country}"
                  li
                    div.field
                      input.form-control type="text" name="job" placeholder="Job" value="#{user.job}"
        div.row.no-gutters
          div.card-body
              div.form-group
                textarea.form-control type="text" name="description" placeholder="Description" value="#{user.description}"

              div.actions
                == submit("Update", class: "btn btn-success btn-sm")
                == link_to("Profile", "/profile", class: "btn btn-light btn-sm")
```

Visualizar todos los usuarios `amberapp\src\views\user\index.slang`

```crystal
div.users-section
  div.user-container
    div.card-deck
      - users.each do |user|
        div.card style="width: 18rem;"
          img.card-img src="/dist/images/default-men.png" height="30" alt="profile logo"
          div.card-body
            h5.card-title == user.name
            p.card-text == user.description
            == link_to("Profile", "/profile", class: "btn btn-light btn-sm")
```

