-- +micrate Up
CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  email VARCHAR,
  name VARCHAR,
  last_name VARCHAR,
  description VARCHAR,
  age INTEGER,
  date_of_birth DATE,
  country VARCHAR,
  job VARCHAR,
  hashed_password VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS users;
