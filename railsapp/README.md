## Running project
```bash
# Start postgresql service
sudo systemctl start postgresql
sudo service postgresql start

# Install dependencies
bundle install
yarn install --check-files

# Setup database
rails db:drop # Delete databases
rails db:reset # Reset database
rails db:create
rails db:migrate

# Running project
rails s
```
