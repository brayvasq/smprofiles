class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?

    private
    def after_sign_in_path_for(resource)
        #edit_user_registration_path
        user_path(resource)
    end

    protected
    def configure_permitted_parameters
        #devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :last_name, :description, :age, :date_of_birth, :country, :job) }
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :description, :age, :date_of_birth, :country, :job, :attachment])
    end
end
