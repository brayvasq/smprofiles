Rails.application.routes.draw do
  devise_for :users
  #resource :users, :only => [:show]
  get 'users/:id' => 'users#show', :as => :user
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "home#index"
  #root "devise/registrations#edit"
  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :relationships,       only: [:create, :destroy]
end
