# Rails App

Instalando ruby en windows

https://gorails.com/setup/windows/10

Template style: https://themes.3rdwavemedia.com/coderpro/bs4/2.0/

## Comandos
```bash
# Crear proyecto
rails new project

# Create model
rails g model profile

# Create controller
rails g controller profiles

# Scaffold
rails g scaffold profiles

# Migration
rails g migration AddFieldToProfile
```

## Creando proyecto

```bash
rails new smprofiles
```

Instalando dependencias

```bash
bundle install
```

Ejecutar proyecto

```bash
rails server
```

Añadiendo remote de Git

```bash
git remote add origin url_proyecto
```

Primer commit 

```bash
git add .
git commit -m "Basic structure"
```

## Añadir Usuarios a la aplicación

Añadir la gema 'devise' al archivo 'Gemfile'

```ruby
gem 'devise'
```

Instalar dependecias

```bash
bundle install
```

Setup devise

```bash
rails g devise:install
```

### Configurar devise

Archivo `config/environments/development.rb` añadir antes del keyword `end`

```ruby
config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

```

En `config\initializers\devise.rb`

```ruby
config.scoped_views = true
```

En el archivo `app/views/layouts/application.html.erb` añadir:

```erb
    <p class="notice"><%= notice %></p>
    <p class="alert"><%= alert %></p>
```

Crear modelo usuario

```bash
rails g devise user
```

Generar vistas

```bash
rails generate devise:views users
```

Generar devise controllers

```bash
rails generate devise:controllers users
```

Generar home controller

```bash
rails generate controller home
```

en config/routes.rb

```rub
root "home#index"

```

en home_controller.rb

```ruby
...
def index
end
...
```

crear el archivo views/home/index.html.erb o en app\views\layouts\application.html.erb

```erb
<p class="navbar-text pull-right">
<% if user_signed_in? %>
  Logged in as <strong><%= current_user.email %></strong>.
  <%= link_to 'Edit profile', edit_user_registration_path, :class => 'navbar-link' %> |
  <%= link_to "Logout", destroy_user_session_path, method: :delete, :class => 'navbar-link'  %>
<% else %>
  <%= link_to "Sign up", new_user_registration_path, :class => 'navbar-link'  %> |
  <%= link_to "Login", new_user_session_path, :class => 'navbar-link'  %>
<% end %>
  <%= link_to "Home", root_path, :class => 'navbar-link'  %>
</p>
```

Configurar redireccion al perfil en 'application_controller.rb'

```ruby
def after_sign_in_path_for(resource)
   edit_user_registration_path
end
```

Añadir accion personalizada

```ruby
def user_profile
end
```

Generando controlador de usuarios

```bash
rails g controller users

```

Añadir al modelo User

```ruby
#attr_accessor :name, :description, :age, :date_of_birth, :country, :job
```

Crear migración

```bash
rails g migration AddFieldsToUser
```

En el archivo de migracion

```ruby
def change
    add_column :users, :name, :string
    add_column :users, :last_name, :string
    add_column :users, :description, :string
    add_column :users, :age, :string
    add_column :users, :date_of_birth, :date
    add_column :users, :country, :string
    add_column :users, :job, :string
  end
```

En users_controllers

```ruby
def show
end
```

En routes

```ruby
devise_for :users  
get 'users/:id' => 'users#show', :as => :user
```

Configurar redireccion al perfil en 'application_controller.rb'

```ruby
def after_sign_in_path_for(resource)
   user_path(resource)
end
```

En application controller

```ruby
before_action :configure_permitted_parameters, if: :devise_controller?
...
    protected
    def configure_permitted_parameters
        #devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :last_name, :description, :age, :date_of_birth, :country, :job) }
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :description, :age, :date_of_birth, :country, :job])
    end

```

Archivo users/show.html.erb

```erb
<div>
<h3>User Info</h3>
<%= @user.name %>
<%= @user.last_name %>
<%= @user.email %>
<%= @user.description %>
<%= @user.age %>
<%= @user.date_of_birth %>
<%= @user.country %>
<%= @user.job %>
</div>

<h3>Cancel my account</h3>

<%= link_to "Back", :back %>

```

Archivo `app\views\devise\registrations\edit.html.erb`

```ruby
<div class="field">
    <%= f.label :name %><br />
    <%= f.text_field :name, autofocus: true, autocomplete: "name" %>
  </div>

  <div class="field">
    <%= f.label :last_name %><br />
    <%= f.text_field :last_name, autofocus: true, autocomplete: "name" %>
  </div>

  <div class="field">
    <%= f.label :description %><br />
    <%= f.text_area :description, autofocus: true, autocomplete: "name" %>
  </div>

  <div class="field">
    <%= f.label :age %><br />
    <%= f.number_field :age, autofocus: true, autocomplete: "name" %>
  </div>

  <div class="field">
    <%= f.label :date_of_birth %><br />
    <%= f.date_field :date_of_birth, autofocus: true, autocomplete: "name" %>
  </div>

  <div class="field">
    <%= f.label :country %><br />
    <%= f.text_field :country, autofocus: true, autocomplete: "name" %>
  </div>

  <div class="field">
    <%= f.label :job %><br />
    <%= f.text_field :job, autofocus: true, autocomplete: "name" %>
  </div>
```

Guardar cambios

```bash
git commit -m "Add Device for user functionality"
```

#### Creando followers

Creando migración

```
rails g model Relationships
```

Buscar la migración create_relationships

```ruby
class CreateRelationships < ActiveRecord::Migration[6.0]
  def change
    create_table :relationships do |t|
      t.integer :follower_id
      t.integer :followed_id

      t.timestamps
    end

    add_index :relationships, :follower_id
    add_index :relationships, :followed_id
    add_index :relationships, [:follower_id, :followed_id], unique: true
  end
end
```

Realizar migraciones con `rails db:migrate`

Create relationships in user model

```ruby
  has_many :active_relationships, class_name:  'Relationship',
           foreign_key: 'follower_id',
           dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed

  has_many :passive_relationships, class_name:  'Relationship',
           foreign_key: 'followed_id',
           dependent:   :destroy


  has_many :followers, through: :passive_relationships, source: :follower

```

Crear relacion en el modelo relationship

```ruby
class Relationship < ApplicationRecord
  belongs_to :follower, class_name: 'User'
  belongs_to :followed, class_name: 'User'
end

```

Generar controlador

```bash
rails g controller Relationships

```

En el controlador Relationships

```ruby
class RelationshipsController < ApplicationController

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
```

En la clase user añadir los siguientes métodos

```ruby
# Follows a user.
  def follow(other_user)
    following << other_user
  end

  # Unfollows a user.
  def unfollow(other_user)
    following.delete(other_user)
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end
```

**Vistas**

Crear vista `views/relationships/create.js.erb`

```erb
document.querySelector("#follow_form").innerHTML = "<%= escape_javascript(render('users/unfollow')) %>"
document.querySelector("#followers").innerHTML = '<%= @user.followers.count %>'
```

Crear vista `views/relationships/destroy.js.erb`

```erb
document.querySelector("#follow_form").innerHTML = "<%= escape_javascript(render('users/follow')) %>"
document.querySelector("#followers").innerHTML = '<%= @user.followers.count %>'

```

Crear en `views/users/_follow_form.html.erb`

```erb
<% unless current_user?(@user) %>
  <div id="follow_form">
    <% if current_user.following?(@user) %>
      <%= render 'unfollow' %>
    <% else %>
      <%= render 'follow' %>
    <% end %>
  </div>
<% end %>

```

Crear en `views/users/_follow.html.erb`

```ruby
<%= form_for(current_user.active_relationships.build, remote: true) do |f| %>
  <div><%= hidden_field_tag :followed_id, @user.id %></div>
  <%= f.submit "Follow", class: "btn btn-primary" %>
<% end %>

```

Crear en `views/users/_unfollow.html.erb`

```ruby
<%= form_for(current_user.active_relationships.find_by(followed_id: @user.id),
             html: { method: :delete },remote: true) do |f| %>
  <%= f.submit "Unfollow", class: "btn" %>
<% end %>

```

Crear vista de estadisticas de followers `shared/_stats.html.erb`

```ruby
<% @user ||= current_user %>
<div class="stats">
  <a href="#">
    <strong id="following" class="stat">
      <%= @user.following.count %>
    </strong>
    following
  </a>
  <a href="#">
    <strong id="followers" class="stat">
      <%= @user.followers.count %>
    </strong>
    followers
  </a>
</div>


```

En la vista `vies/users/show.html.erb`

```ruby
<section class="stats">
  <%= render 'shared/stats' %>
</section>

<%= render 'follow_form' %>

```

En el helper 'users_helper.rb'

```ruby
module UsersHelper
  # Returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end
end

```

Listar followers y following.

Crear vista de estadisticas de followers `shared/_stats.html.erb`

```ruby
<% @user ||= current_user %>
<div class="stats">
  <a href="<%= following_user_path(@user) %>">
    <strong id="following" class="stat">
      <%= @user.following.count %>
    </strong>
    following
  </a>
  <a href="<%= followers_user_path(@user) %>">
    <strong id="followers" class="stat">
      <%= @user.followers.count %>
    </strong>
    followers
  </a>
</div>

```

En el controlador de usuarios user_controller.rb

```ruby
def following
        @title = 'Following'
        @user  = User.find(params[:id])
        @users = @user.following
        render 'show_follow'
    end

    def followers
        @title = 'Followers'
        @user  = User.find(params[:id])
        @users = @user.followers
        render 'show_follow'
    end
```

En el archivo `config/routes.rb`

```ruby
resources :users do
    member do
      get :following, :followers
    end
  end

resources :relationships,       only: [:create, :destroy]
```

Crear el archivo `views/users/show_follow.html.erb`

```ruby
<% provide(:title, @title) %>
<div class="row">
  <aside class="col-md-4">
    <section class="user_info">
      <h1><%= @user.name %></h1>
      <span><%= link_to "view my profile", @user %></span>
    </section>
    <section class="stats">
      <%= render 'shared/stats' %>
      <% if @users.any? %>
        <div class="user_avatars">
          <% @users.each do |user| %>
            <%= link_to  user.name, user %>
          <% end %>
        </div>
      <% end %>
    </section>
  </aside>
  <div class="col-md-8">
    <h3><%= @title %></h3>
    <% if @users.any? %>
      <ul class="users follow">
        <%= render @users %>
      </ul>
    <% end %>
  </div>
</div>
```

Para que la instrucción `render @users` funcione se debe crear un partial para los usuarios `views/users/_user.html.erb`

```erb
<li>
  <%= link_to user.name, user %>
</li>

```

Guardar cambios

```bash
git commit -m "Add follow feature"

```

#### Image profile

https://gitlab.kommit.co/rails/sample_app/blob/master/Gemfile

Instalar las siguientes gemas

```ruby
gem 'carrierwave'
gem 'mini_magick'

```

Ejecutar `bundle install`

Es posible que se necesita instalar lo siguiente en ubuntu

```bash
sudo apt-get update
sudo apt-get install libmagickwand-dev
sudo apt-get install imagemagick

```

Crear migración

```bash
rails g migration AddAttachmentFieldsToUser attachment:string

```

Debería quedar algo como lo siguiente

```ruby
class AddAttachmentFieldsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :attachment, :string
  end
end

```

Realizar migraciones con `rails db:migrate`

Generar uploader

```bash
rails g uploader attachment

```

Configurar Uploader, añadir lo siguiente

```ruby
include CarrierWave::MiniMagick
...
def extension_whitelist
  %w(jpg jpeg gif png)
end

```

Ir al model user y añadir lo siguiente:

```ruby
mount_uploader :attachment, AttachmentUploader # Tells rails to use this uploader for this model.   
   validates :name, presence: true # Make sure the owner's name is present.   

```

Ir a la vista edit.html.erb de devise

```erb
<div class="field">
    <%= f.label :attachment %><br />
    <%= f.file_field :attachment, autofocus: true, autocomplete: "attachment" %>
 </div>

```

En la vista show.html.erb de users añadir

```erb
<img src="<%= @user.attachment_url  %>" alt="">

```

En el application_controller.rb añadir el parametro attachment al parameter_sanitizer

```ruby
def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :description, :age, :date_of_birth, :country, :job, :attachment])
    end

```

Añadir lo siguiente al uploader

```ruby
process resize_to_limit: [400, 400]

```

Guardando cambios

```bash
git commit -m "Add Upload feature"

```

## Integrando bootstrap

Instalando bootstrap con yarn y webpack

```bash
yarn add bootstrap jquery popper.js
```

Revisar las versiones instaladas en el archivo `app/package.json`

Configurar alias para Jquery y Popper en el archivo `config/webpack/environment.js`

```javascript
const {environment} = require('@rails/webpacker')

const webpack = require('webpack')

environment.plugins.append('Provide',
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        Popper: ['popper.js', 'default']
    })
)

module.exports = environment
```

Importar bootstrap en `app/javascript/packs/application.js`

```javascript
import 'bootstrap'

document.addEventListener("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
})
```

Añadir en `app/javascript/stylesheets/application.scss`

```scss
@import "~bootstrap/scss/bootstrap";

```

Anadir en `config/webpacker.yml`

```yaml
resolved_paths: ['app/assets']

```

Importar archivo scss en ``app/javascript/packs/application.js``

```javascript
import '../stylesheets/application'

```

##### Mejorando las vistas

https://blog.prototypr.io/create-landing-pages-like-stripe-does-9d7b81e8babd

Creando navegación

En el archivo `app/views/layouts/application.html.erb`

```erb
<!DOCTYPE html>
<html>
<head>
  <title>Smprofiles</title>
  <%= csrf_meta_tags %>
  <%= csp_meta_tag %>

  <%= stylesheet_link_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' %>
  <%= javascript_pack_tag 'application', 'data-turbolinks-track': 'reload' %>
</head>

<body>
<header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <%= image_tag("RubyIcon.png", :class => "navbar-image") %>
      SmProfiles
    </a>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">

        <li class="nav-item active">
          <%= link_to "Home", root_path, :class => 'nav-link' %>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Features</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Pricing</a>
        </li>
      </ul>

    </div>
    <div>
      <% if user_signed_in? %>
        <!--        Logged in as <strong><%#= current_user.email %></strong>.-->
        <!--        <%#= link_to 'Edit profile', edit_user_registration_path, :class => 'navbar-link' %> |-->
        <%= link_to 'Show profile', current_user, :class => 'btn btn-sm' %>
        <%= link_to "Logout", destroy_user_session_path, method: :delete, :class => 'btn btn-danger btn-sm' %>
      <% else %>
        <%= link_to "Sign up", new_user_registration_path, :class => 'btn btn-sm' %> |
        <%= link_to "Login", new_user_session_path, :class => 'btn btn-danger btn-sm' %>
      <% end %>
    </div>
  </nav>
</header>

<p class="notice"><%= notice %></p>
<p class="alert"><%= alert %></p>
<%= yield %>


<footer class="py-4 bg-light text-black-50">
  <div class="container text-center">
    <small>Copyright &copy; Your Website</small>
  </div>
</footer>
</body>
</html>
```

En el archivo de estilos `app/assets/stylesheets/application.scss`

```scss
/*
 * This is a manifest file that'll be compiled into application.css, which will include all the files
 * listed below.
 *
 * Any CSS and SCSS file within this directory, lib/assets/stylesheets, or any plugin's
 * vendor/assets/stylesheets directory can be referenced here using a relative path.
 *
 * You're free to add application-wide styles to this file and they'll appear at the bottom of the
 * compiled file so the styles you add here take precedence over styles defined in any other CSS/SCSS
 * files in this directory. Styles in this file should be added after the last require_* statement.
 * It is generally better to create a new file per style scope.
 *
 *= require_tree .
 *= require_self
 */
@import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');

html{
  font-family: 'Montserrat', sans-serif;
}

.navbar{
  background-color: rgb(240, 242, 245);
  box-shadow: rgba(0, 0, 0, 0.15) 0px 1px 0px 0px;
  height: 36px;
  position: fixed !important;
  width: inherit !important;
}

.btn-sm{
  width: 100px;
}

.navbar-image{
  width: 24px;
  height: 24px;
  border-radius: 100%;
  margin-top: -5px;
  margin-left: -5px;
  margin-right: 5px;
}

.btn:hover, .btn:focus {
  border-color: #2E2F30;
}

header{
  z-index: 1;
  position: absolute;
  width: 100%;
  top: 0;
}

.my-container{
  display: flex;
  justify-content: center;
  align-content: center;
  padding: 100px;
}

.my-container .card{
  width: 100%;
}

.users-section h3, .user-container a{
  text-align: center;
}

.user-container {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5%;
}

.more-profiles{
  position: relative;
  top: 50%;
  display: flex !important;
  justify-content: center;
  align-items: center;
}


@media (width: 963px) {
  height: auto !important;
}
```

En el archivo home `app/views/home/index.html.erb`

```erb
<section>
  <div class="block"></div>
  <div class="text">
    <h1>Become known, get your profile</h1>
    <p> Smtodo is an application where you can describe yourself and explore profile of million of users.
    </p>

    <div class="">
      <%= link_to "Get Started", new_user_registration_path, :class => 'btn btn-info' %>
    </div>
  </div>

  <div class="users-section">
    <h3>Some Users</h3>
    <div class="user-container">
      <div class="card-deck">
        <% @users.each do |user| %>
          <%= render user unless user.name.blank? || user.description.blank? %>
        <% end %>

        <div class="card" style="width: 18rem;">
          <div class="card-body">
            <%= link_to "Explore Users", users_path, :class => 'btn btn-info more-profiles' %>
          </div>
        </div>

      </div>
    </div>
  </div>

</section>
```

En el archivo de estilos `app/assets/stylesheets/home.scss`

```scss
// Place all the styles related to the home controller here.
// They will automatically be included in application.scss.
// You can use Sass (SCSS) here: http://sass-lang.com/

.block{
  width: 100%;
  height: 700px;
  background: linear-gradient(150deg, #dc3545 , #ffffff);
  position: absolute;
}

.text {
  position: relative;
  height: 760px;
  display: flex;
  margin: -60px 0 0 150px;
  flex-direction: column;
  justify-content: center;
  color: #ffffff;
  max-width: 1024px;
}
.text h1 {
  font-size: 40px;
  opacity: 1;
}
.text p {
  color: #ffffff;
  font-size: 20px;
  max-width: 50%;
  font-size: 17px;
  line-height: 28px;
  opacity: 1;
}
```

Inicio de sesión `app/views/devise/sessions/new.html.erb`

```erb
<section>
  <div class="container my-container">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Log In</h5>
        <div class="col-md-6 col-xs-6 col-sm-12 col-6 float-left">
          <%= form_for(resource, as: resource_name, url: session_path(resource_name)) do |f| %>
            <div class="form-group">
              <%= f.label :email %><br />
              <%= f.email_field :email, autofocus: true, autocomplete: "email", class: 'form-control'%>
            </div>

            <div class="form-group">
              <%= f.label :password %><br />
              <%= f.password_field :password, autocomplete: "current-password", class: 'form-control' %>
            </div>

            <% if devise_mapping.rememberable? %>
              <div class="form-group">
                <%= f.check_box :remember_me %>
                <%= f.label :remember_me %>
              </div>
            <% end %>

            <div class="actions">
              <%= f.submit "Log in", class: 'btn btn-danger' %>
              <%= render "devise/shared/links" %>
            </div>
          <% end %>

        </div>
        <div class="col-md-6 col-xs-6 col-sm-6 col-6 float-right">
          <div>
            <h5>Your password is your master encryption key.</h5>
            <p>
              Your account’s security depends on the strength of your password. Passwords that are too short, too simple or consist of dictionary words are easily guessed.</p>
          </div>
          <div>
            <h5>Your account is only as secure as your computer.</h5>
            <p>
              Never enter your password on a device that you do not fully trust. Do not log into your account from a shared or public computer. For enhanced security, install our browser extension and load MEGA from your computer rather than from our servers.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
```

Registro de usuario `app/views/devise/registrations/new.html.erb`

```erb
<section>
  <div class="container my-container">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Sign Up</h5>
        <div class="col-md-6 col-xs-6 col-sm-12 col-6 float-left">
          <%= form_for(resource, as: resource_name, url: registration_path(resource_name)) do |f| %>
            <%= render "devise/shared/error_messages", resource: resource %>

            <div class="form-group">
              <%= f.label :email %><br />
              <%= f.email_field :email, autofocus: true, autocomplete: "email", class: 'form-control'%>
            </div>

            <div class="form-group">
              <%= f.label :password %>
              <% if @minimum_password_length %>
                <em>(<%= @minimum_password_length %> characters minimum)</em>
              <% end %><br />
              <%= f.password_field :password, autocomplete: "new-password", class: 'form-control'%>
            </div>

            <div class="form-group">
              <%= f.label :password_confirmation %><br />
              <%= f.password_field :password_confirmation, autocomplete: "new-password", class: 'form-control'%>
            </div>

            <div>
              <%= f.submit "Sign up", class: 'btn btn-danger'%>

              <%= render "devise/shared/links" %>
            </div>
          <% end %>
        </div>
        <div class="col-md-6 col-xs-6 col-sm-6 col-6 float-right">
          <div>
            <h5>Your password is your master encryption key.</h5>
            <p>
              Your account’s security depends on the strength of your password. Passwords that are too short, too simple or consist of dictionary words are easily guessed.</p>
          </div>
          <div>
            <h5>Your account is only as secure as your computer.</h5>
            <p>
Never enter your password on a device that you do not fully trust. Do not log into your account from a shared or public computer. For enhanced security, install our browser extension and load MEGA from your computer rather than from our servers.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

```

User profile `app/views/users/show.html.erb`

```erb
<section>
  <div class="container my-container col-md-8">
    <div class="card" >
      <div class="row no-gutters">
        <div class="col-md-4">
          <% if @user.attachment_url %>
            <img src="<%= @user.attachment_url %>" class='card-img' alt="">
          <% else %>
            <%= image_tag("default-men.png", :class => "card-img") %>
          <% end %>
        </div>
        <div class="col-md-8">
          <div class="card-body">
            <div class="basic-info">
              <h5 class="card-title">Basic info</h5>
              <ul class="list-group list-group-flush">
                <li class="list-group-item"> <p><b>Name : </b>  <%=  @user.name %></p></li>
                <li class="list-group-item"> <p><b>Last Name : </b><%= @user.last_name %></p></li>
                <li class="list-group-item"> <p><b>Email : </b><%= @user.email %></p></li>
                <li class="list-group-item"> <p><b>Age : </b><%= @user.age %></p></li>
                <li class="list-group-item"> <p><b>Date of Birth : </b><%= @user.date_of_birth %></p></li>
                <li class="list-group-item"> <p><b>Country : </b><%= @user.country %></p></li>
                <li class="list-group-item"> <p><b>Job : </b><%= @user.job %></p></li>
                <li class="list-group-item">  <%= link_to "Download", @user.attachment_url %> </li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="card-body">
          <div class="">
            <%= render 'shared/stats' %>
            <%= render 'follow_form' %>
          </div>
          <div>
            <h5>Description</h5>
            <p><%= @user.description %></p>
          </div>
          <div class="">
            <%= link_to 'Edit profile', edit_user_registration_path, class: 'btn btn-outline-warning' %>
            <%= link_to "Back", :back, class: 'btn btn-outline-info'%>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

```

Estilos usuario `app/assets/stylesheets/users.scss`

```scss
// Place all the styles related to the users controller here.
// They will automatically be included in application.scss.
// You can use Sass (SCSS) here: http://sass-lang.com/

.basic-info .list-group-item{
  padding: 0;
}

.div-actions{
  float: right;
}

.edit-list li{
  list-style-type: none;
}

.user-img{
  height: 256px;
}

```

User edit `app/views/devise/registrations/edit.html.erb`

```erb
<section>
  <div class="container my-container col-md-8">
    <div class="card">
      <%= form_for(resource, as: resource_name, url: registration_path(resource_name), html: {method: :put}) do |f| %>
        <div class="row no-gutters">
          <div class="col-md-4">
            <% if resource.attachment_url %>
              <img src="<%= resource.attachment_url %>" alt="">
            <% else %>
              <%= image_tag("default-men.png", :class => "card-img") %>
            <% end %>
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <div class="basic-info">
                <h5 class="card-title">Edit <%= resource_name.to_s.humanize %></h5>
                <%= render "devise/shared/error_messages", resource: resource %>

                <ul class="list-group list-group-flush edit-list">
                  <li class="">
                    <div class="field">

                    </div>
                    <div class="input-group mb-3">
                      <div class="custom-file">
                        <%= f.label :attachment, class: 'custom-file-label' %><br/>
                        <%= f.file_field :attachment, autofocus: true, autocomplete: "attachment", class: 'custom-file-input' %>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="inputGroupFileAddon02">Upload</span>
                      </div>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :name %><br/>
                      <%= f.text_field :name, autofocus: true, autocomplete: "name", class: 'form-control' %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :last_name %><br/>
                      <%= f.text_field :last_name, autofocus: true, autocomplete: "name", class: "form-control" %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :email %><br/>
                      <%= f.email_field :email, autofocus: true, autocomplete: "email", class: "form-control" %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :age %><br/>
                      <%= f.number_field :age, autofocus: true, autocomplete: "name", class: "form-control" %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :date_of_birth %><br/>
                      <%= f.date_field :date_of_birth, autofocus: true, autocomplete: "name", class: "form-control" %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :country %><br/>
                      <%= f.text_field :country, autofocus: true, autocomplete: "name", class: "form-control" %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= f.label :job %><br/>
                      <%= f.text_field :job, autofocus: true, autocomplete: "name", class: "form-control" %>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="row no-gutters col-md-12">
            <div class="card-body">
              <div>
                <h5>Description</h5>
                <div class="form-group">
                  <%= f.label :description %><br/>
                  <%= f.text_area :description, autofocus: true, autocomplete: "name", class: 'form-control' %>
                </div>
              </div>

              <div class="">
                <% if devise_mapping.confirmable? && resource.pending_reconfirmation? %>
                  <div>Currently waiting confirmation for: <%= resource.unconfirmed_email %></div>
                <% end %>

                <div class="form-group">
                  <%= f.label :password %> <i>(leave blank if you don't want to change it)</i><br/>
                  <%= f.password_field :password, autocomplete: "new-password", class: "form-control" %>
                  <% if @minimum_password_length %>
                    <br/>
                    <em><%= @minimum_password_length %> characters minimum</em>
                  <% end %>
                </div>

                <div class="form-group">
                  <%= f.label :password_confirmation %><br/>
                  <%= f.password_field :password_confirmation, autocomplete: "new-password", class: "form-control" %>
                </div>

                <div class="form-group">
                  <%= f.label :current_password %> <i>(we need your current password to confirm your changes)</i><br/>
                  <%= f.password_field :current_password, autocomplete: "current-password", class: "form-control" %>
                </div>

                <div class="actions float-right">
                  <%= f.submit "Update", class: 'btn btn-outline-warning' %>
                </div>
              </div>
            </div>
          </div>
        </div>
      <% end %>
      <div class="row no-gutters col-md-12">
        <div class="card-body">
          <div class="actions">
            <h3>Cancel my account</h3>

            <p>Unhappy? <%= button_to "Cancel my account", registration_path(resource_name), data: {confirm: "Are you sure?"}, method: :delete, class: 'btn btn-outline-danger' %></p>

            <%= link_to "Back", :back, class: 'btn btn-outline-info float-right' %>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

```

Partial user `app/views/users/_user.html.erb`

```erb
<div class="card" style="width: 18rem;">
  <% if user.attachment_url %>
    <img src="<%= user.attachment_url %>" class='card-img-top user-img' alt="">
  <% else %>
    <%= image_tag("default-men.png", :class => "card-img-top user-img") %>
  <% end %>
  <div class="card-body">
    <h5 class="card-title"><%= user.name %> </h5>
    <p class="card-text"> <%= user.description.split(".")[0] if user.description %></p>
    <%= link_to 'View Profile', user, class: 'btn btn-info' %>
  </div>
</div>

```

Partial list user `app/views/users/_users.html.erb`

```erb
<div class="users-section">
  <h3><%= @title %></h3>
  <div class="user-container">
    <div class="card-deck">
      <% @users.each do |user| %>
        <%= render user unless user.name.blank? || user.description.blank? %>
      <% end %>
    </div>
  </div>
</div>

```

Show followers `app/views/users/show_follow.html.erb`

```erb
<% provide(:title, @title) %>
<div class="row">
  <aside class="col-md-12">
    <%= render 'users' %>
  </aside>
</div>

```

Show all users `app/views/users/index.html.erb`

```erb
<%= render 'users' %>

```

En el controlador de usuario añadir la acción index `app/controllers/users_controller.rb`

```ruby
def index
        @title = "Users"
        @user = current_user
        @users = User.all
end
```

