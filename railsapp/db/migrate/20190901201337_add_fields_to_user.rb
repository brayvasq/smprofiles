class AddFieldsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :name, :string
    add_column :users, :last_name, :string
    add_column :users, :description, :string
    add_column :users, :age, :string
    add_column :users, :date_of_birth, :date
    add_column :users, :country, :string
    add_column :users, :job, :string
  end
end
