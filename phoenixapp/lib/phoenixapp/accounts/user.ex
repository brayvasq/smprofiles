defmodule Phoenixapp.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Phoenixapp.Accounts.{User, Encryption}

  schema "users" do
    field :email, :string
    field :encrypted_password, :string
    field :name, :string
    field :last_name, :string
    field :description, :string
    field :age, :string
    field :date_of_birth, :date
    field :country, :string
    field :job, :string

    # VIRTUAL FIELDS
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :name, :last_name, :description, :age, :date_of_birth, :country, :job])
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> validate_format(:email, ~r/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)
    |> validate_required([:email])
    |> unique_constraint(:email)
    |> downcase_email
    |> encrypt_password
  end

  def encrypt_password(changeset) do
    IO.inspect(changeset)
    password = get_change(changeset, :password)
    if password do
      encrypted_password = Encryption.hash_password(password)
      IO.inspect(password)
      IO.inspect(encrypted_password)
      put_change(changeset, :encrypted_password, encrypted_password)
    else
      changeset
    end
  end

  defp downcase_email(changeset) do
    update_change(changeset, :email, &String.downcase/1)
  end
end
