defmodule PhoenixappWeb.PageController do
  use PhoenixappWeb, :controller

  alias Phoenixapp.Accounts
  alias Phoenixapp.Accounts.User

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end
end
