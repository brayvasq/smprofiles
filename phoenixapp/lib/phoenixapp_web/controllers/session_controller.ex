defmodule PhoenixappWeb.SessionController do
  use PhoenixappWeb, :controller

  alias Phoenixapp.Accounts.Auth
  alias Phoenixapp.Repo

  def new(conn, _params) do
    render(conn, "new.html")
  end

  #@spec create(Plug.Conn.t().map()) :: Plug.Conn.t()
  def create(conn, %{"session" => auth_params}) do
    case Auth.login(auth_params, Repo) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "Signed in successfully.")
        |> redirect(to: Routes.user_path(conn, :show, user))
      :error ->
        conn
        |> put_flash(:error, "There was a problem with your username/password")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> delete_session(:current_user_id)
    |> put_flash(:info, "Signed out successfully.")
    |> redirect(to: Routes.session_path(conn, :new))
  end
end