defmodule PhoenixappWeb.Router do
  use PhoenixappWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PhoenixappWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/", PhoenixappWeb do
    pipe_through [:browser]#, PhoenixappWeb.Plugs.Guest] # Para restringir acceso
                                                        # Pero daña la navegación

    resources "/register", UserController, only: [:create, :new]
    #resources "/register", User
    get "/login", SessionController, :new
    post "/login", SessionController, :create
  end

  scope "/", PhoenixappWeb do
    pipe_through [:browser]#, PhoenixappWeb.Plugs.Auth] # Para restringir acceso
                                                        # Pero daña la navegación

    delete "/logout", SessionController, :delete
    resources "/users", UserController, only: [:index, :edit, :show, :update, :delete]
  end

  # Other scopes may use custom stacks.
  # scope "/api", PhoenixappWeb do
  #   pipe_through :api
  # end
end
