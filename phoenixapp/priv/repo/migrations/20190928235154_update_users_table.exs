defmodule Phoenixapp.Repo.Migrations.UpdateUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :name, :string
      add :last_name, :string
      add :description, :string
      add :age, :string
      add :date_of_birth, :date
      add :country, :string
      add :job, :string
    end
  end
end
