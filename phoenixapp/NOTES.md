# Phoenixapp

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

# Guide
### Phoenix App

Instalando elixir

https://elixir-lang.org/install.html

Instalando Phoenix

https://hexdocs.pm/phoenix/installation.html#content

**Creando proyecto**

```bash
mix phx.new elixirapp
```

Cuando pregunte lo siguiente

```bash
Fetch and install dependencies? [Yn]
```

Elegir no y entrar a la carpeta de la aplicación.

```bash
cd phoenixapp
```

**Nota: ** En caso de dar sí y la instalación de dependencias se congele, se debe cancelar la operación Ctrl+c, ingresar a la carpeta y continuar con la guía.

https://elixirforum.com/t/phx-new-hangs-at-running-mix-deps-get/20770/4

------------------------------- OMITIR --------------------------------------

**Nota:** para trabajar con SqlLite se debe añadir la dependencia `sqlite_ecto` en el archivo `phoenixapp/mix.exs`

http://twentyeighttwelve.com/creating-a-phoenix-framework-application-with-sqlite/

```elixir
def application do
    [
      mod: {Phoenixapp.Application, []},
      extra_applications: [:logger, :runtime_tools, :sqlite_ecto2]
    ]
end

defp deps do
  [...
   {:sqlite_ecto2, "~> 2.2"},
   ...]
end
```

Actualizar el archivo `phoenixapp/config/dev.exs` para añadir el adaptador [Sqlite.Ecto](https://github.com/jazzyb/sqlite_ecto)

https://github.com/elixir-sqlite/sqlite_ecto2

```elixir
config :phoenixapp, Phoenixapp.Repo,
       adapter: Sqlite.Ecto2,
       database: "db/phoenixapp.sqlite"
```

En el archivo `phoenixapp/lib/phoenixapp/repo.ex`

```elixir
defmodule Phoenixapp.Repo do
  use Ecto.Repo,
    otp_app: :phoenixapp,
    adapter: Sqlite.Ecto2
end
```

------------------------------- OMITIR --------------------------------------

Instalar dependencias

```bash
mix deps.get
cd assets && npm install && node node_modules/webpack/bin/webpack.js --mode development
cd ..
```

En caso de que la instalación falle se puede ejecutar lo siguiente  para desbloquear las versiones de las dependencias

```bash
mix deps.unlock --all
```

Después de eso volver a instalar las dependencias

Setup database

Por defecto Phoenix trabaja con postgresql, así que se debe tener instalada la base de datos.

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04

sudo /usr/lib/postgresql/10/bin/pg_ctl -D /var/lib/postgresql/10/main -l logfile start

```bash
mix ecto.create
```

**Nota:** por si el comando anterior saca error con la dependencia  rebar3: https://github.com/dwyl/learn-phoenix-framework/issues/121

O descargar `https://repo.hex.pm/installs/1.0.0/rebar3-3.5.1` renombar el fichero como rebar3 y ejecutar `mix local.rebar rebar3 ./rebar3`

Ejecutar proyecto

```bash
mix phx.server
```

Ejecutar dentro de un shell de elixir

```bash
iex -S mix phx.server
```

**Nota** : mix deps.update --all --> Equivalente de bundle update

### Añadir Usuarios a la aplicación

---- COHERENCE NO soporta Ecto 3 ---------------------------

Añadir la dependencia [Coherence](https://github.com/smpallen99/coherence) en el archivo `phoenixapp/mix.exs`

```elixir
def deps do
  [{:coherence, "~> 0.5.2"}]
end
```

Añadir coherence a la sección application del archivo `phoenixapp/mix.exs`

```elixir
def application do
    [
      mod: {Phoenixapp.Application, []},
      extra_applications: [..., :coherence]
    ]
end
```

Instalar dependecia

```bash
mix deps.get
```

---- COHERENCE NO soporta Ecto 3 ---------------------------

Para la autenticación seguiré el tutorial de https://medium.com/innoventes/user-authentication-phoenix-1-4-ebda68016740

Creando recurso Usuarios

```bash
mix phx.gen.html Accounts User users email:string:unique encrypted_password:string
```

El comando anterior crea todo lo necesario para trabajar con el usuario, Vistas, Controller, Migración, Modelo etc.

Editar el archivo `phoenixapp/lib/phoenixapp/accounts/user.ex` y añadir campos virtuales para las contraseñas.

```elixir
	# VIRTUAL FIELDS
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
```

Añadir dependencias para encriptar contraseñas

```elixir
defp deps do
  [
  ....
  
      {:comeonin, "~> 4.0"},
      {:bcrypt_elixir, "~> 1.0"},
  ]
end
```

Instalar dependencias

```bash
mix deps.get
```

Crear un modulo para encriptar las contraseñas`phoenixapp/lib/phoenixapp/accounts/encryption.ex`

```elixir
defmodule Chirper.Accounts.Encryption do
  alias Comeonin.Bcrypt
  alias Phoenixapp.Accounts.User

  def hash_password(password), do: Bcrypt.hashpwsalt(password)

  def validate_password(%User{} = user, password), do: Bcrypt.check_pass(user, password)
end
```

Actualizar el modelo (Schema user) para añadir validaciones `phoenixapp/lib/phoenixapp/accounts/user.ex`

```elixir
defmodule Phoenixapp.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Phoenixapp.Accounts.{User, Encryption}

  schema "users" do
	...
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password])
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> validate_format(:email, ~r/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)
    |> validate_required([:email])
    |> unique_constraint(:email)
    |> downcase_email
    |> encrypt_password
  end

  def encrypt_password(changeset) do
    password = get_change(changeset, :password)
    if password do
      encrypted_password = Encryption.hash_password(password)
      put_change(changeset, :encrypted_password, encrypted_password)
    else
      changeset
    end
  end

  defp downcase_email(changeset) do
    update_change(changeset, :email, &String.downcase/1)
  end
end
```

Actualizar el formulario del usuario

```elixir
<%= form_for @changeset, @action, fn f -> %>
  <%= if @changeset.action do %>
    <div class="alert alert-danger">
      <p>Oops, something went wrong! Please check the errors below.</p>
    </div>
  <% end %>

  <%= label f, :email %>
  <%= text_input f, :email %>
  <%= error_tag f, :email %>

  <%= password_input f, :password, placeholder: "Password" %>
  <%= error_tag f, :password %>

  <%= password_input f, :password_confirmation, placeholder: "Confirm Password" %>
  <%= error_tag f, :password_confirmation %>
  <div>
    <%= submit "Save" %>
  </div>
<% end %>
```

Actualizar Controlador de usuario `phoenixapp/lib/phoenixapp_web/controllers/user_controller.ex` y cambiar la acción create

```elixir
def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: Routes.user_path(conn, :show, user))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
```

Añadir rutas de usuario al archivo `phoenixapp/lib/phoenixapp_web/router.ex`

```elixir
scope "/", PhoenixappWeb do
    pipe_through :browser

    resources "/register", UserController, only: [:create, :new]
    get "/login", SessionController, :new
    post "/login", SessionController, :create
    delete "/logout", SessionController, :delete

    get "/", PageController, :index
end
```

Crear el modulo `phoenixapp/lib/phoenixapp/accounts/auth.ex`

```elixir
defmodule Phoenixapp.Accounts.Auth do
  alias Phoenixapp.Accounts.{Encryption, User}

  def login(params, repo) do
    user = repo.get_by(User, email: String.downcase(params["email"]))

    case authenticate(user, params["password"]) do
      true -> {:ok, user}
      _    -> :error
    end
  end

  defp authenticate(user, password) do
    if user do
      {:ok, authenticated_user} = Encryption.validate_password(user, password)
      authenticated_user.email == user.email
    else
      nil
    end
  end

  def signed_in?(conn) do
    conn.assigns[:current_user]
  end
end
```

importar el modulo Auth en `phoenixapp/lib/phoenixapp_web.ex`

```elixir
defmodule PhoenixappWeb do
  ....
  def view do
    quote do
      ....
      import Phoenixapp.Accounts.Auth, only: [signed_in?: 1]
    end
  end
end
```

importando el método `signed_in?/1` se habilita el uso del mismo en todos los views

Crear el controlador de sesiones `phoenixapp/lib/phoenixapp_web/controllers/session_controller.ex` 

```elixir
defmodule PhoenixappWeb.SessionController do
  use PhoenixappWeb, :controller

  alias Phoenixapp.Accounts.Auth
  alias Phoenixapp.Repo

  def new(conn, _params) do
    render(conn, "new.html")
  end

  #@spec create(Plug.Conn.t().map()) :: Plug.Conn.t()
  def create(conn, %{"session" => auth_params}) do
    case Auth.login(auth_params, Repo) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "Signed in successfully.")
        |> redirect(to: Routes.user_path(conn, :show, user))
      :error ->
        conn
        |> put_flash(:error, "There was a problem with your username/password")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> delete_session(:current_user_id)
    |> put_flash(:info, "Signed out successfully.")
    |> redirect(to: Routes.session_path(conn, :new))
  end
end
```

Crear el view de usuario `phoenixapp/lib/phoenixapp_web/views/session_view.ex`

```elixir
defmodule PhoenixappWeb.SessionView do
  use PhoenixappWeb, :view
end
```

Crear template para inicio de session

```elixir
<div class='row'>
  <div class='col s12 m6 offset-m3'>
    <div class='card'>
      <div class='card-content'>
        <span class='card-title'>LOGIN</span>
          <div class='row card-form'>
          <%= form_for @conn, Routes.session_path(@conn, :new), [as: :session], fn f -> %>
            <%= text_input f, :email, placeholder: "Email" %>
            <%= password_input f, :password, placeholder: "Password" %>
            <%= submit "Submit", class: "btn right secondary-color" %>
            <%= link "Register", to: Routes.user_path(@conn, :new), class: "btn secondary-color" %>
          <% end %>
          </div>
      </div>
    </div>
  </div>
</div>
```

Ejecutar migraciones

```bash
mix ecto.migrate
```

Ver rutas

```bash
mix phx.routes
```

Ejecutar proyecto

```bash
mix phx.server
```

Crear plug para restringir rutas `phoenixapp/lib/phoenixapp_web/plugs/auth.ex`

```elixir
defmodule PhoenixappWeb.Plugs.Auth do
  import Plug.Conn
  import Phoenix.Controller

  alias Phoenixapp.Accounts

  def init(opts), do: opts

  def call(conn, _opts) do
    if user_id = Plug.Conn.get_session(conn, :current_user_id) do
      current_user = Accounts.get_user!(user_id)
      conn
        |> assign(:current_user, current_user)
    else
      conn
        |> redirect(to: "/login")
        |> halt()
    end
  end
end
```

El Plug anterior redirecciona a la ruta login si el usuario no está logueado, si el usario está logueado asigna el :current_user

En le archivo `phoenixapp/lib/phoenixapp_web/plugs/guest.ex`

```elixir
defmodule PhoenixappWeb.Plugs.Guest do
  import Plug.Conn
  import Phoenix.Controller

  def init(opts), do: opts

  def call(conn, _opts) do
    if Plug.Conn.get_session(conn, :current_user_id) do
      conn
      |> redirect(to: PhoenixappWeb.Router.Helpers.page_path(conn, :index))
      |> halt()
    end
    conn
  end
end
```

El plug anterior redirecciona al home si el usuario ya está logueado y accede a las rutas sign_in, login

Añadir los plugs al archivo `phoenixapp/lib/phoenixapp_web/router.ex`

```elixir
scope "/", PhoenixappWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/", PhoenixappWeb do
    pipe_through [:browser]#, PhoenixappWeb.Plugs.Guest] # Para restringir acceso
                                                        # Pero daña la navegación

    resources "/register", UserController, only: [:create, :new]
    #resources "/register", User
    get "/login", SessionController, :new
    post "/login", SessionController, :create
  end

  scope "/", PhoenixappWeb do
    pipe_through [:browser]#, PhoenixappWeb.Plugs.Auth] # Para restringir acceso
                                                        # Pero daña la navegación

    delete "/logout", SessionController, :delete
    resources "/users", UserController, only: [:index, :edit, :show, :update, :delete]
  end
```

Modificar el layout `phoenixapp/lib/phoenixapp_web/templates/layout/app.html.eex`

```elixir
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Phoenixapp · Phoenix Framework</title>
    <link rel="stylesheet" href="<%= Routes.static_path(@conn, "/css/app.css") %>"/>
  </head>
  <body>
    <header>
      <section class="container">
        <nav role="navigation">
              <div class="nav-wrapper primary-dark-color">
                <a href="/" class="brand-logo">CHIRPER</a>
             <ul>
                         <li><a href="https://hexdocs.pm/phoenix/overview.html">Get Started</a></li>
                       </ul>
                <!-- Dropdown Structure User actions -->
                                    <%= if signed_in?(@conn) do %>

                <ul id="user-actions" class="dropdown-content">
                  <li><%= link "Logout", to: Routes.session_path(@conn, :delete), method: :delete %></li>
                </ul>
                    <% end %>

                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <%= if signed_in?(@conn) do %>
                      <li><a href='#' >Some place</a></li>
                      <li><a class="dropdown-trigger" href="#!" data-target="user-actions"> <%= @current_user.username %> <i class="material-icons right">arrow_drop_down</i></a></li>
                    <% end %>
                </ul>
              </div>
            </nav>

            <main>
              <p class="alert alert-info" role="alert"><%= get_flash(@conn, :info) %></p>
              <p class="alert alert-danger" role="alert"><%= get_flash(@conn, :error) %></p>
              <div class='container'>
                <%= render @view_module, @view_template, assigns %>
              </div>
            </main>
        <a href="http://phoenixframework.org/" class="phx-logo">
          <img src="<%= Routes.static_path(@conn, "/images/phoenix.png") %>" alt="Phoenix Framework Logo"/>
        </a>
      </section>
    </header>
    <script type="text/javascript" src="<%= Routes.static_path(@conn, "/js/app.js") %>"></script>
  </body>
</html>
```

Crear migración para añadir campos al modelo users

```bash
mix ecto.gen.migration update_users_table
```

En la migración `phoenixapp/priv/repo/migrations/00000_update_users_table.exs`

```elixir
defmodule Phoenixapp.Repo.Migrations.UpdateUsersTable do
  use Ecto.Migration

  def change do
    add :name, :string
      add :last_name, :string
      add :description, :string
      add :age, :string
      add :date_of_birth, :date
      add :country, :string
      add :job, :string
  end
end
```

En el modelo users `phoenixapp/lib/phoenixapp/accounts/user.ex`

```elixir
schema "users" do
    ...
    field :name, :string
    field :last_name, :string
    field :description, :string
    field :age, :string
    field :date_of_birth, :date
    field :country, :string
    field :job, :string
 	...
end

 @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :name, :last_name, :description, :age, :date_of_birth, :country, :job])
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> validate_format(:email, ~r/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)
    |> validate_required([:email])
    |> unique_constraint(:email)
    |> downcase_email
    |> encrypt_password
  end
```

Ejecutar migraciones

```bash
mix ecto.migrate
```

### Integrando bootstrap

https://medium.com/wecode-ng/how-to-install-bootstrap-on-phoenix-1-4-project-c8aa724dcdeb

```bash
cd assets
npm install — save bootstrap jquery popper.js font-awesome
```

renombrar `phoenixapp/assets/css/app.css` por `phoenixapp/assets/css/app.scss`  y `phoenixapp/assets/css/phoenix.css` por `phoenixapp/assets/css/phoenix.scss`

Ir a **assets/js/app.js** e importar 'bootstrap'

```javascript
import 'bootstrap';
import css from app.css
```

Ir a **assets/css/app.scss** e importar 'bootstrap'

```css
@import "../node_modules/bootstrap/dist/css/bootstrap.css";
@import "../node_modules/font-awesome/css/font-awesome.css";
```

Ir a **assets/webpack.config.js** y añadir lo siguiente

```javascript
{
    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
    use: [{
        loader: 'file-loader',
        options: {
            name: '[name].[ext]',
            outputPath: '../fonts'
        }
    }]
}
```

Añadir la configuración para compilar scss

```javascript
{
        test: /\.s?css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
```

https://experimentingwithcode.com/using-bootstrap-and-sass-in-phoenix/

https://www.rockyourcode.com/phoenix-1-4-with-bootstrap-j-query-bootstrap-notify-sass-and-webpack --> Funciona

### Mejorando Vistas

Layout de la página `phoenixapp/lib/phoenixapp_web/templates/layout/app.html.eex`, Continene Header y Footer.

```elixir
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Phoenixapp · Phoenix Framework</title>
    <link rel="stylesheet" href="<%= Routes.static_path(@conn, "/css/app.css") %>"/>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
              <img src="<%= Routes.static_path(@conn, "/images/phoenixicon.png") %>" alt="Phoenix Framework Logo" class="navbar-image"/>
              SmProfiles
         </a>
         <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav">

                 <li class="nav-item active">
                     <%= link "Home", to: Routes.page_path(@conn, :index), class: 'nav-link' %>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="#">Features</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="#">Pricing</a>
                 </li>
               </ul>
         </div>
         <div>
               <%= if signed_in?(@conn) do %>
                 <%= link "Show profile", to: Routes.user_path(@conn, :show, @current_user), class: 'btn btn-sm' %>
                 <%= link "Logout", to: Routes.session_path(@conn, :delete), method: :delete, class: 'btn btn-sm btn-primary' %>
               <% else %>
                 <%= link "Sign up", to: Routes.user_path(@conn, :new), class: 'btn btn-sm' %>
                 <%= link "Login", to: Routes.session_path(@conn, :new), class: 'btn btn-sm btn-primary' %>
               <% end %>
         </div>
      </nav>
    </header>
    <main>
        <%= render @view_module, @view_template, assigns %>
    </main>
    <footer class="py-4 bg-light text-black-50">
      <div class="container text-center">
        <small>Copyright &copy; Your Website</small>
      </div>
    </footer>
    <script type="text/javascript" src="<%= Routes.static_path(@conn, "/js/app.js") %>"></script>
  </body>
</html>
```

Css principal, Los estilos van a estar solo en un archivo. `phoenixapp/assets/css/app.scss`

```scss
/* This file is for your main application css. */

$fa-font-path: '~font-awesome/fonts';
@import '~bootstrap/scss/bootstrap.scss';
@import '~font-awesome/scss/font-awesome.scss';

$primary: #9b59b6;
$secondary: #8e44ad;

.btn-primary {
  background-color: $primary;
  border-color: $primary;
}

.btn-primary:hover, .btn-primary:focus{
  background-color: $secondary;
  border-color: $secondary;
}

.navbar{
  background-color: rgb(240, 242, 245);
  box-shadow: rgba(0, 0, 0, 0.15) 0px 1px 0px 0px;
  height: 36px;
  position: fixed !important;
  width: inherit !important;
}

.btn-sm{
  width: 100px;
}

.navbar-image{
  width: 24px;
  height: 24px;
  border-radius: 100%;
  margin-top: -5px;
  margin-left: -5px;
  margin-right: 5px;
}

header{
  z-index: 1;
  position: absolute;
  width: 100%;
  top: 0;
}

.my-container{
  display: flex;
  justify-content: center;
  align-content: center;
  padding: 100px;
}

.my-container .card{
  width: 100%;
}

.users-section h3, .user-container a{
  text-align: center;
}

.user-container {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5%;
}

.more-profiles{
  position: relative;
  top: 50%;
  display: flex !important;
  justify-content: center;
  align-items: center;
  width: 50%;
}

.block{
  width: 100%;
  height: 700px;
  background: linear-gradient(150deg, $primary , #ffffff);
  position: absolute;
}

.basic-info .list-group-item{
  padding: 0;
}

.div-actions{
  float: right;
}

.edit-list li{
  list-style-type: none;
}

.user-img{
  height: 256px;
}

.text {
  position: relative;
  height: 760px;
  display: flex;
  margin: -60px 0 0 150px;
  flex-direction: column;
  justify-content: center;
  color: #ffffff;
  max-width: 1024px;
}
.text h1 {
  font-size: 40px;
  opacity: 1;
}
.text p {
  color: #ffffff;
  font-size: 20px;
  max-width: 50%;
  font-size: 17px;
  line-height: 28px;
  opacity: 1;
}
```

Página principal `phoenixapp/lib/phoenixapp_web/templates/page/index.html.eex`

```elixir
<section>
  <div class="block"></div>
  <div class="text">
    <h1>Become known, get your profile</h1>
    <p> Smtodo is an application where you can describe yourself and explore profile of million of users.
    </p>

    <div class="">
      <%= link "Get Started", to: Routes.user_path(@conn, :new), class: 'btn btn-info more-profiles' %>
    </div>
  </div>

  <div class="users-section">
    <h3>Some Users</h3>
    <div class="user-container">
       <%= link "Explore Users", to: Routes.user_path(@conn, :index), class: 'btn btn-info more-profiles' %>
    </div>
  </div>

</section>
```

Vista de session `phoenixapp/lib/phoenixapp_web/templates/session/new.html.eex`

```elixir
<section>
  <div class="container my-container">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Log In</h5>
        <div class="col-md-6 col-xs-6 col-sm-12 col-6 float-left">
          <%= form_for @conn, Routes.session_path(@conn, :new), [as: :session], fn f -> %>
            <div class="form-group">
              <%= text_input f, :email,placeholder: "Email", class: 'form-control'%>
            </div>

            <div class="form-group">
               <%= password_input f, :password, placeholder: "Password", class: 'form-control' %>
            </div>


            <div class="actions">
              <%= submit "Log In", class: "btn btn-sm btn-primary" %>
              <%= link "Sign Up", to: Routes.user_path(@conn, :new), class: "btn btn-sm" %>
            </div>
          <% end %>

        </div>
        <div class="col-md-6 col-xs-6 col-sm-6 col-6 float-right">
          <div>
            <h5>Your password is your master encryption key.</h5>
            <p>
              Your account’s security depends on the strength of your password. Passwords that are too short, too simple or consist of dictionary words are easily guessed.</p>
          </div>
          <div>
            <h5>Your account is only as secure as your computer.</h5>
            <p>
              Never enter your password on a device that you do not fully trust. Do not log into your account from a shared or public computer. For enhanced security, install our browser extension and load MEGA from your computer rather than from our servers.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

```

Vista de creación de usuario Sign up `phoenixapp/lib/phoenixapp_web/templates/user/new.html.eex`

```elixir
<section>
  <div class="container my-container">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Log In</h5>
        <div class="col-md-6 col-xs-6 col-sm-12 col-6 float-left">
          <%= form_for @conn, Routes.user_path(@conn, :create), [as: :user], fn f -> %>
            <div class="form-group">
              <%= text_input f, :email,placeholder: "Email", class: 'form-control'%>
              <%= error_tag f, :email %>
            </div>

            <div class="form-group">
               <%= password_input f, :password, placeholder: "Password", class: 'form-control' %>
               <%= error_tag f, :password %>
            </div>

            <div class="form-group">
                 <%= password_input f, :password_confirmation, placeholder: "Confirm Passwor", class: 'form-control' %>
                 <%= error_tag f, :password_confirmation %>
            </div>

            <div class="actions">
              <%= submit "Sign Up", class: "btn btn-sm btn-primary" %>
              <%= link "Log In", to: Routes.session_path(@conn, :new), class: "btn btn-sm" %>
            </div>
          <% end %>

        </div>
        <div class="col-md-6 col-xs-6 col-sm-6 col-6 float-right">
          <div>
            <h5>Your password is your master encryption key.</h5>
            <p>
              Your account’s security depends on the strength of your password. Passwords that are too short, too simple or consist of dictionary words are easily guessed.</p>
          </div>
          <div>
            <h5>Your account is only as secure as your computer.</h5>
            <p>
              Never enter your password on a device that you do not fully trust. Do not log into your account from a shared or public computer. For enhanced security, install our browser extension and load MEGA from your computer rather than from our servers.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
```

Perfil del usuario `phoenixapp/lib/phoenixapp_web/templates/user/show.html.eex`

```elixir
<section>
  <div class="container my-container col-md-8">
    <div class="card" >
      <div class="row no-gutters">
        <div class="col-md-4">
              <img src="<%= Routes.static_path(@conn, "/images/default-men.png") %>" alt="Logo" class="card-img"/>
        </div>
        <div class="col-md-8">
          <div class="card-body">
            <div class="basic-info">
              <h5 class="card-title">Basic info</h5>
              <ul class="list-group list-group-flush">
                <li class="list-group-item"> <p><b>Name : </b>  <%=  @user.name %></p></li>
                <li class="list-group-item"> <p><b>Last Name : </b><%= @user.last_name %></p></li>
                <li class="list-group-item"> <p><b>Email : </b><%= @user.email %></p></li>
                <li class="list-group-item"> <p><b>Age : </b><%= @user.age %></p></li>
                <li class="list-group-item"> <p><b>Date of Birth : </b><%= @user.date_of_birth %></p></li>
                <li class="list-group-item"> <p><b>Country : </b><%= @user.country %></p></li>
                <li class="list-group-item"> <p><b>Job : </b><%= @user.job %></p></li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="card-body">
          <div>
            <h5>Description</h5>
            <p><%= @user.description %></p>
          </div>
          <div class="">
            <%= link "Edit profile", to: Routes.user_path(@conn, :edit, @user), class: 'btn btn-outline-warning' %>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
```

Vista de edición de usuario `phoenixapp/lib/phoenixapp_web/templates/user/edit.html.eex`

```
<section>
  <div class="container my-container col-md-8">
    <div class="card">
      <%= form_for @conn, Routes.user_path(@conn, :update, @user), [method: :put, as: :user], fn f -> %>
        <div class="row no-gutters">
          <div class="col-md-4">
             <img src="<%= Routes.static_path(@conn, "/images/default-men.png") %>" alt="Logo" class="card-img"/>
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <div class="basic-info">
                <h5 class="card-title">Edit <%= @user.name %></h5>

                <ul class="list-group list-group-flush edit-list">
                  <li class="">
                    <div class="field">
                      <%= text_input f, :name, placeholder: "Name", class: 'form-control'%>
                      <%= error_tag f, :name %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= text_input f, :last_name, placeholder: "Last Name", class: 'form-control'%>
                      <%= error_tag f, :last_name %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= text_input f, :email, placeholder: "Email", class: 'form-control'%>
                      <%= error_tag f, :email %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= text_input f, :age, placeholder: "Age", class: 'form-control'%>
                      <%= error_tag f, :age %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= date_input f, :date_of_birth, placeholder: "Date of Birth", class: 'form-control'%>
                      <%= error_tag f, :date_of_birth %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= text_input f, :country, placeholder: "Country", class: 'form-control'%>
                      <%= error_tag f, :country %>
                    </div>
                  </li>
                  <li class="">
                    <div class="field">
                      <%= text_input f, :job, placeholder: "Job", class: 'form-control'%>
                      <%= error_tag f, :job %>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="row no-gutters col-md-12">
            <div class="card-body">
              <div>
                <h5>Description</h5>
                <div class="form-group">
                 <%= textarea f, :description, placeholder: "Description", class: 'form-control'%>
                 <%= error_tag f, :description %>
                </div>
              </div>

              <div class="">
                <div class="form-group">
                    <%= password_input f, :password, placeholder: "Password", class: 'form-control' %>
                    <%= error_tag f, :password %>
                </div>

                <div class="form-group">
                    <%= password_input f, :password_confirmation, placeholder: "Confirm Password", class: 'form-control' %>
                    <%= error_tag f, :password_confirmation %>
                </div>

                <div class="actions float-right">
                  <%= submit "Update", class: "btn btn-outline-warning" %>
                </div>
              </div>
            </div>
          </div>
        </div>
      <% end %>
      <div class="row no-gutters col-md-12">
        <div class="card-body">
          <div class="actions">
            <h3>Cancel my account</h3>

            <p> <%= link "Cancel my account", to: Routes.user_path(@conn, :delete, @user), method: :delete, class: 'btn btn-sm btn-outline-danger', data: [confirm: "Are you sure?"] %> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
```

Vista para listar usuarios `phoenixapp/lib/phoenixapp_web/templates/user/index.html.eex`

```elixir
<div class="users-section">
  <div class="user-container">
    <div class="card-deck">
        <%= for user <- @users do %>
            <div class="card" style="width: 18rem;">
              <img src="<%= Routes.static_path(@conn, "/images/default-men.png") %>" alt="Logo" class="card-img"/>
              <div class="card-body">
                <h5 class="card-title"><%= user.name %> </h5>
                <p class="card-text"> <%= user.description %></p>
                <%= link "Show", to: Routes.user_path(@conn, :show, user) %>
              </div>
            </div>
        <% end %>
    </div>
  </div>
</div>
```
